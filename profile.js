let CURRENT_USER = ''; // IMPORTANT THIS IS THE USER THAT WILL GET ACCESS TO HIS PROFILE


// ********************************************* NAVBAR ****************************************** 
const navToggle = document.querySelector(".nav-toggle");
const links = document.querySelector(".links");

const dashboardBtn = document.querySelector('.user-profile');
dashboardBtn.addEventListener('click', () => { window.location.href = `dashboard.html` });


const LOGOUT_BTN = document.querySelector(".logout-btn");
LOGOUT_BTN.addEventListener("click", logout);
function logout() {
  let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
  parsedJsonObj.loggedIn = false;
  localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
  window.location.href = "login.html";
}


navToggle.addEventListener('click', function () {
  links.classList.toggle('show-links');
})


// ***************************************** CURRENT TIME ******************************************

function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  // add a zero in front of numbers<10
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('time').innerHTML = h + " : " + m + " : " + s;
  t = setTimeout(function () {
    startTime()
  }, 500);
}
startTime();


// ***************************************** PROFILE PAGE ******************************************

const container = document.querySelector(".pending-task-container");

const list = document.querySelector(".pending-task-list"); // list where tasks will be added

const form = document.querySelector(".profile-edit-form");
let formControl = document.querySelector(".form-control");
const task = document.getElementById("task"); // task input

const alert = document.querySelector(".alert");

window.addEventListener("DOMContentLoaded", checkAuth);

form.addEventListener("submit", addItem); // submit form

function isNumberKey(evt) {
  var charCode = (evt.which) ? evt.which : evt.keyCode
  return !(charCode > 31 && (charCode < 48 || charCode > 57));
}


function checkAuth() {
  let count = 0;
  Object.keys(localStorage).forEach((key, index) => {
    if (JSON.parse(localStorage[key]).loggedIn === true) {
      CURRENT_USER = key;
      // alert(`Welcome ${JSON.parse(localStorage[key]).userName}`);

      window.onload = () => {
        setupItems(); // display added items onload
      }
      count = count + 1;
    }
  })
  if (count === 0) {
    window.location.href = 'login.html';
  }
}

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

let editElement;
let editFlag = false;
let editKEY = "";

function addItem(e) {
  e.preventDefault();

  let value = task.value; // this gets the input value

  if (editKEY === "userName") {

    let task = document.getElementById("task");
    value = task.value;
    successfullyAdded();
  }

  if (editKEY === "email") {
    let task = document.getElementById("task");
    value = task.value;

    let doesTheEmailExistCount = 0;
    Object.keys(localStorage).forEach((key, index) => {
      if (value === JSON.parse(localStorage[key]).email) {
        doesTheEmailExistCount = doesTheEmailExistCount + 1;
        displayAlert("Email is already in use", "danger");
      }
    })
    if (doesTheEmailExistCount === 0) {
      if (!validateEmail(value)) {
        displayAlert("Enter a proper email! (i.e. ex@ex.com)", "danger");
      }
      else {
        successfullyAdded();
      }
    }
  }
  if (editKEY === "mobileNumber") {
    let task = document.getElementById("task");
    value = task.value;
    splittedValArray = value.split('');
    const filteredSplittedValArray = splittedValArray.filter(el => {
      return Number.isNaN(el);
    });
    function isNumber(n) {
      return !isNaN(parseFloat(n)) && !isNaN(n - 0);
    }

    let doesTheNumberExistCount = 0;
    Object.keys(localStorage).forEach((key, index) => {
      if (value === JSON.parse(localStorage[key]).mobileNumber) {
        doesTheNumberExistCount = doesTheNumberExistCount + 1;
        displayAlert("Mobile Number is Taken", "danger");
      }
    })
    if (doesTheNumberExistCount === 0) {
      if (value.length !== 10) {
        displayAlert("Enter correct 10 digit mobile number!", "danger");
      }
      else if (((value[0] === '9') || (value[0] === '8') || (value[0] === '7')) && isNumber(value)) {
        successfullyAdded();
      }
      else if (!isNumber(value)) {
        displayAlert("Enter numeric values only!", "danger");
      }
      else {
        displayAlert("Please Enter A valid Mobile Number", "danger");
      }
    }
  }
  if (editKEY.toLowerCase() === "gender") {
    let task = document.getElementById("task");
    value = task.value;
    successfullyAdded();
  }
  if (editKEY === "dob") {
    let task = document.getElementById("task");
    value = task.value;

    const dobStringARRAY = value.split("-");
    filteredDobStringARRAY = dobStringARRAY.filter(el => {
      return Number.isNaN(el);
    })
    function leapYear(year) {
      return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }
    if (filteredDobStringARRAY.length > 0) {
      displayAlert("DOB Must have only numeric values", "danger");
    }
    else if ((value.indexOf('-') !== 4) || (value.lastIndexOf('-') !== 7)) {
      displayAlert("Please enter DOB in YYYY-MM-DD format", "danger");
    }
    else if (parseInt(value.slice(0, 4)) < 1970) {
      displayAlert("Birth Year cannot be less than 1970", "danger");
    }
    else if (parseInt(value.slice(0, 4)) > 2005) {
      displayAlert("Birth Year cannot be greater than 2005", "danger");
    }
    else if (parseInt(value.slice(5, 7)) > 12) {
      displayAlert("Birth Month cannot be Greater than 12", "danger");
    }
    else if (parseInt(value.slice(5, 7)) < 01) {
      displayAlert("Birth Month cannot be less than 1", "danger");
    }
    else if (parseInt(value.slice(8,)) > 31) {
      displayAlert("Birth day cannot be greater than 31", "danger");
    }
    else if (parseInt(value.slice(8,)) < 01) {
      displayAlert("Birth day cannot be less than 1", "danger");
    }
    else if (((parseInt(value.slice(5, 7)) === 04) || (parseInt(value.slice(5, 7)) === 06) || (parseInt(value.slice(5, 7)) === 08) || (parseInt(value.slice(5, 7)) === 10) || (parseInt(value.slice(5, 7)) === 12)) && (parseInt(value.slice(8,)) > 30)) {
      displayAlert("Birth day cannot be greater than 30 for entered Month", "danger");
    }
    else if ((leapYear(parseInt(value.slice(0, 4)))) && (parseInt(value.slice(5, 7)) === 02) && (parseInt(value.slice(8,)) > 29)) {
      displayAlert("Birth day cannot be greater than 29 for entered Month & Year", "danger");
    }
    else if ((!leapYear(parseInt(value.slice(0, 4)))) && (parseInt(value.slice(5, 7)) === 02) && (parseInt(value.slice(8,)) > 28)) {
      displayAlert("Birth day cannot be greater than 28 for entered Month & Year", "danger");
    }
    else { successfullyAdded() }
  }
  if (editKEY === "password") {
    let task = document.getElementById("task");
    value = task.value;
    const regex = /\d/;
    if (value.length < 8) {
      displayAlert("password should have Minimum 8 characters", "danger");
    }
    else if ((!regex.test(value))) {
      displayAlert("password should have Minimum one number", "danger");
    }
    else {
      successfullyAdded();
    }
  }

  function successfullyAdded() {
    if (value !== "" && editFlag) { // EDIT mode
      const firstSlice = editElement.innerHTML.slice(0, editElement.innerHTML.indexOf(':') + 2);

      if (editKEY === 'dob') {
        const year = value.slice(0, 4);
        const month = value.slice(5, 7);
        const day = value.slice(8,);
        editValue = `${day}-${month}-${year}`;

        editElement.innerHTML = firstSlice.concat(editValue);
      }

      else {
        editElement.innerHTML = firstSlice.concat(value)
      }
      displayAlert("Edit Successful", "success");

      editLocalStorage(editKEY, value); // edit  local storage
      formControl.classList.remove("visible-form-control");
      setBackToDefault();

    } else { // when input box is blank
      displayAlert("please enter a value", "danger");
    }
  }

}

// display alert
function displayAlert(text, action) {
  alert.textContent = text;
  alert.classList.add(`alert-${action}`);
  // remove alert
  setTimeout(function () {
    alert.textContent = "";
    alert.classList.remove(`alert-${action}`);
  }, 2000);
}


function getLocalStorage() {
  return JSON.parse(localStorage.getItem(CURRENT_USER));
}

function editLocalStorage(key, value) {
  if (key === "password") {
    value = encryptionFunction(value);

    let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
    parsedJsonObj[`${key}`] = `${value}`;
    localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
  }
  else {
    let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
    parsedJsonObj[`${key}`] = `${value}`;
    localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
  }

}


function setupItems() {

  let items = getLocalStorage(CURRENT_USER);
  let usefulItemsObject = {};
  for (const [key, value] of Object.entries(items)) {
    if ((key !== "list") && (key !== "completedList") && (key !== "loggedIn")) {

      usefulItemsObject[`${key}`] = `${value}`;
    }
  }

  Object.keys(usefulItemsObject).forEach((key, index) => {

    if (key === "password") {
      const value = decryptionFunction(usefulItemsObject[`${key}`]);
      createListItem(`${key}`, `${value}`);
    }
    else {
      const value = usefulItemsObject[`${key}`];
      createListItem(`${key}`, `${value}`);
    }
  })

  container.classList.add("show-container");
}

function createListItem(key, value) {
  const element = document.createElement("article");
  let attr = document.createAttribute("data-key");
  attr.value = key;
  element.setAttributeNode(attr);
  element.classList.add("task-item");
  if (key === "dob") {
    const year = value.slice(0, 4);
    const month = value.slice(5, 7);
    const day = value.slice(8,);
    value = `${day}-${month}-${year}`;

    element.innerHTML = (`
    <p class="title">${key}: ${value}</p>
    <div class="btn-container">
      <!-- edit btn -->
      <button type="button" class="edit-btn">
        <i class="fas fa-edit"></i>
      </button>
    </div>
  `);
  }
  else {
    element.innerHTML = (`
    <p class="title">${key}: ${value}</p>
    <div class="btn-container">
      <!-- edit btn -->
      <button type="button" class="edit-btn">
        <i class="fas fa-edit"></i>
      </button>
    </div>
  `);
  }
  // add event listeners to edit buttons;
  const editBtn = element.querySelector(".edit-btn");
  editBtn.addEventListener("click", editItem);

  // append child
  list.appendChild(element);

}

function editItem(e) {
  formControl.classList.add("visible-form-control");
  // console.log(formControl.removeChild(formControl.children[0]));
  // console.log(formControl);

  const element = e.currentTarget.parentElement.parentElement;// selects the article

  if (element.dataset["key"] === "gender") {
    formControl.innerHTML = "";
    formControl.innerHTML = (`
      <select name="gender" id="task" class="inputs">
					<option hidden disabled selected value>-- select an option --</option>
					<option value="male">Male</option>
					<option value="female">Female</option>
			</select>
      <button type="submit" class="submit-btn">Edit</button>
    `);
    let task = document.getElementById("task");
    editElement = e.currentTarget.parentElement.previousElementSibling;

    task.value = editElement.innerHTML.slice(editElement.innerHTML.indexOf(':') + 2,); //sets input box value to the editElement^ value
    editFlag = true;
    editKEY = element.dataset.key;
  }
  else if (element.dataset["key"] === "dob") {
    formControl.innerHTML = "";

    formControl.innerHTML = (`
      <input
					type="date"
					value=""
					placeholder="dd-mm-yyyy"
					min="1970-01-01"
					max="2005-12-31"
					id="task"
					class="inputs"
				/>
        <button type="submit" class="submit-btn">Edit</button>
    `);

    let task = document.getElementById("task");

    editElement = e.currentTarget.parentElement.previousElementSibling;

    const defValue = editElement.innerHTML.slice(editElement.innerHTML.indexOf(':') + 2,);
    console.log(defValue);

    const year = defValue.slice(6,);
    const month = defValue.slice(3, 5);
    const day = defValue.slice(0, 2);
    defaultValueProperty = `${year}-${month}-${day}`;
    console.log(defaultValueProperty);

    var att = document.createAttribute("value");
    att.value = `${year}-${month}-${day}`;
    task.setAttributeNode(att);

    // task.value = editElement.innerHTML.slice(editElement.innerHTML.indexOf(':') + 2,); //sets input box value to the editElement^ value
    editFlag = true;
    editKEY = element.dataset.key;
  }
  else if (element.dataset["key"] === "mobileNumber") {
    formControl.innerHTML = "";
    formControl.innerHTML = (`
      <input
						type="text"
						id="task"
						class="inputs"
						maxlength="10"
						onkeypress="return isNumberKey(event);"
					/>
					<button type="submit" class="submit-btn">Edit</button>
    `);
    let task = document.getElementById("task");
    editElement = e.currentTarget.parentElement.previousElementSibling;
    task.value = editElement.innerHTML.slice(editElement.innerHTML.indexOf(':') + 2,); //sets input box value to the editElement^ value
    editFlag = true;
    editKEY = element.dataset.key;
  }
  else if (element.dataset["key"] === "userName") {
    formControl.innerHTML = "";
    formControl.innerHTML = (`
      <input type="text" id="task" placeholder="Edit Profile" />
			<button type="submit" class="submit-btn">Edit</button>
    `);
    let task = document.getElementById("task");
    editElement = e.currentTarget.parentElement.previousElementSibling;

    task.value = editElement.innerHTML.slice(editElement.innerHTML.indexOf(':') + 2,); //sets input box value to the editElement^ value
    editFlag = true;
    editKEY = element.dataset.key;
  }
  else if (element.dataset["key"] === "email") {
    formControl.innerHTML = "";
    formControl.innerHTML = (`
      <input type="email" id="task" class="inputs" />
      <button type="submit" class="submit-btn">Edit</button>
    `);

    let task = document.getElementById("task");
    editElement = e.currentTarget.parentElement.previousElementSibling;

    task.value = editElement.innerHTML.slice(editElement.innerHTML.indexOf(':') + 2,); //sets input box value to the editElement^ value
    editFlag = true;
    editKEY = element.dataset.key;
  }
  else if (element.dataset["key"] === 'password') {
    formControl.innerHTML = "";
    formControl.innerHTML = (`
      <input 
        type="password" 
        id="task" class="inputs"
        placeholder="Password must NOT contain ';' or backslash/back-ticks  or single/double quotes"
      />
			<button type="submit" class="submit-btn">Edit</button>
    `);

    // set edit item
    editElement = e.currentTarget.parentElement.previousElementSibling;

    task.value = editElement.innerHTML.slice(editElement.innerHTML.indexOf(':') + 2,); //sets input box value to the editElement^ value
    editFlag = true;
    editKEY = element.dataset.key;
  }
}

function setBackToDefault() {
  task.value = "";
  editFlag = false;
  editKEY = "";
}





function encryptionFunction(enteredPassword) {
  const passwordArray = enteredPassword.split('');
  let encryptedPasswordArray = [];

  passwordArray.forEach((letter) => {
    switch (letter) {
      case "a":
        encryptedPasswordArray.push('01');
        break;
      case "b":
        encryptedPasswordArray.push('02');
        break;
      case "c":
        encryptedPasswordArray.push('03');
        break;
      case "d":
        encryptedPasswordArray.push('04');
        break;
      case "e":
        encryptedPasswordArray.push('05');
        break;
      case "f":
        encryptedPasswordArray.push('06');
        break;
      case "g":
        encryptedPasswordArray.push('07');
        break;
      case "h":
        encryptedPasswordArray.push('08');
        break;
      case "i":
        encryptedPasswordArray.push('09');
        break;
      case "j":
        encryptedPasswordArray.push('10');
        break;
      case "k":
        encryptedPasswordArray.push('11');
        break;
      case "l":
        encryptedPasswordArray.push('12');
        break;
      case "m":
        encryptedPasswordArray.push('13');
        break;
      case "n":
        encryptedPasswordArray.push('14');
        break;
      case "o":
        encryptedPasswordArray.push('15');
        break;
      case "p":
        encryptedPasswordArray.push('16');
        break;
      case "q":
        encryptedPasswordArray.push('17');
        break;
      case "r":
        encryptedPasswordArray.push('18');
        break;
      case "s":
        encryptedPasswordArray.push('19');
        break;
      case "t":
        encryptedPasswordArray.push('20');
        break;
      case "u":
        encryptedPasswordArray.push('21');
        break;
      case "v":
        encryptedPasswordArray.push('22');
        break;
      case "w":
        encryptedPasswordArray.push('23');
        break;
      case "x":
        encryptedPasswordArray.push('24');
        break;
      case "y":
        encryptedPasswordArray.push('25');
        break;
      case "z":
        encryptedPasswordArray.push('26');
        break;

      case '1':
        encryptedPasswordArray.push('aa');
        break;
      case '2':
        encryptedPasswordArray.push('bb');
        break;
      case '3':
        encryptedPasswordArray.push('cc');
        break;
      case '4':
        encryptedPasswordArray.push('dd');
        break;
      case '5':
        encryptedPasswordArray.push('ee');
        break;
      case '6':
        encryptedPasswordArray.push('ff');
        break;
      case '7':
        encryptedPasswordArray.push('gg');
        break;
      case '8':
        encryptedPasswordArray.push('hh');
        break;
      case '9':
        encryptedPasswordArray.push('ii');
        break;
      default:
        encryptedPasswordArray.push(letter);
      // code block
    }
  })
  const encryptedPasswordString = encryptedPasswordArray.join(';');
  return encryptedPasswordString;
}


function decryptionFunction(encryptedPasswordStr) {
  const passwordArray = encryptedPasswordStr.split(';');
  let decryptedPasswordArray = [];

  passwordArray.forEach((letter) => {
    switch (letter) {
      case "01":
        decryptedPasswordArray.push('a');
        break;
      case "02":
        decryptedPasswordArray.push('b');
        break;
      case "03":
        decryptedPasswordArray.push('c');
        break;
      case "04":
        decryptedPasswordArray.push('d');
        break;
      case "05":
        decryptedPasswordArray.push('e');
        break;
      case "06":
        decryptedPasswordArray.push('f');
        break;
      case "07":
        decryptedPasswordArray.push('g');
        break;
      case "08":
        decryptedPasswordArray.push('h');
        break;
      case "09":
        decryptedPasswordArray.push('i');
        break;
      case "10":
        decryptedPasswordArray.push('j');
        break;
      case "11":
        decryptedPasswordArray.push('k');
        break;
      case "12":
        decryptedPasswordArray.push('l');
        break;
      case "13":
        decryptedPasswordArray.push('m');
        break;
      case "14":
        decryptedPasswordArray.push('n');
        break;
      case "15":
        decryptedPasswordArray.push('o');
        break;
      case "16":
        decryptedPasswordArray.push('p');
        break;
      case "17":
        decryptedPasswordArray.push('q');
        break;
      case "18":
        decryptedPasswordArray.push('r');
        break;
      case "19":
        decryptedPasswordArray.push('s');
        break;
      case "20":
        decryptedPasswordArray.push('t');
        break;
      case "21":
        decryptedPasswordArray.push('u');
        break;
      case "22":
        decryptedPasswordArray.push('v');
        break;
      case "23":
        decryptedPasswordArray.push('w');
        break;
      case "24":
        decryptedPasswordArray.push('x');
        break;
      case "25":
        decryptedPasswordArray.push('y');
        break;
      case "26":
        decryptedPasswordArray.push('z');
        break;
      case 'aa':
        decryptedPasswordArray.push('1');
        break;
      case 'bb':
        decryptedPasswordArray.push('2');
        break;
      case 'cc':
        decryptedPasswordArray.push('3');
        break;
      case 'dd':
        decryptedPasswordArray.push('4');
        break;
      case 'ee':
        decryptedPasswordArray.push('5');
        break;
      case 'ff':
        decryptedPasswordArray.push('6');
        break;
      case 'gg':
        decryptedPasswordArray.push('7');
        break;
      case 'hh':
        decryptedPasswordArray.push('8');
        break;
      case 'ii':
        decryptedPasswordArray.push('9');
        break;
      default:
        decryptedPasswordArray.push(letter);
      // code block
    }
  })
  const decryptedPasswordString = decryptedPasswordArray.join('');
  return decryptedPasswordString;
}