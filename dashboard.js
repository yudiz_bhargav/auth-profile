let CURRENT_USER = ''; // IMPORTANT THIS IS THE USER THAT WILL GET ACCESS TO HIS PROFILE


// ********************************************* NAVBAR ****************************************** 
const navToggle = document.querySelector(".nav-toggle");
const links = document.querySelector(".links");

const profileUserName = document.querySelector('.user-profile');
profileUserName.addEventListener('click', () => { window.location.href = profile.html });
// console.log(profileUserName.innerText);

const LOGOUT_BTN = document.querySelector(".logout-btn");
LOGOUT_BTN.addEventListener("click", logout);
function logout() {
  let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
  parsedJsonObj.loggedIn = false;
  localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
  window.location.href = "login.html";
}
// window.onbeforeunload = function () {
//   let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
//   parsedJsonObj.loggedIn = false;
//   localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
//   location.href = "login.html";
// };


navToggle.addEventListener('click', function () {
  links.classList.toggle('show-links')
})


// ***************************************** CURRENT TIME ******************************************

function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  // add a zero in front of numbers<10
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('time').innerHTML = h + " : " + m + " : " + s;
  t = setTimeout(function () {
    startTime()
  }, 500);
}
startTime();

// ************************************************ WEATHER *************************************************
window.addEventListener('load', () => {
  let long;
  let lat;
  let temperatureDescription = document.querySelector('.temperature-description');
  let temperatureDegree = document.querySelector('.temperature-degree');
  let locationTimezone = document.querySelector('.location-timezone');
  let locationIcon = document.querySelector('.weather-icon');
  let temperatureSection = document.querySelector('.degree-section');
  let temperatureSpan = document.querySelector('.degree-section span');

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
      long = position.coords.longitude;
      lat = position.coords.latitude;

      const apiKey = '5306c8e86913fb7be9a01368fe3397f9';

      //add this proxy right before the https of api string when in development mode
      // const proxy = `https://cors-anywhere.herokuapp.com/`;

      const api = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${apiKey}`;

      fetch(api)
        .then(response => {
          return response.json();
        })
        .then(data => {
          console.log(data);
          const { temp } = data.main;
          const temperature = (temp - 273).toFixed(2);
          const { description } = data.weather[0];
          const { icon } = data.weather[0];
          const { country } = data.sys;

          // Formula for Fahrenheit  
          let fahrenheit = ((temperature * 1.8) + 32).toFixed(2);

          // set DOM elements from the API
          temperatureDegree.textContent = temperature;
          temperatureDescription.textContent = description;
          locationTimezone.textContent = `${data.name}(${country})`;
          locationIcon.innerHTML = `<img src="weatherIcons/${icon}.png">`;

          // Change temp to Celsius/Fahrenheit 
          temperatureSection.addEventListener('click', () => {
            if (temperatureSpan.textContent === 'C') {
              temperatureSpan.textContent = "F";
              temperatureDegree.textContent = fahrenheit;
            }
            else {
              temperatureSpan.textContent = "C";
              temperatureDegree.textContent = temperature;
            }
          })
        })
    })
  } else {
    h1.textContent = "Weather Unavailable / Location Not Fetched";
  }

})

// ***************************************** RANDOM GIF GENERATOR ******************************************
let gifContainer = document.querySelector('.gif-container');

let refresh;
const duration = 1000 * 10; // duration count in seconds = 2

const giphy = {
  baseURL: "https://api.giphy.com/v1/gifs/random",
  apiKey: "9lUeSFuYrnTdKTgn5hcpJLXKDgt91XbJ",
  tag: "fail",
  rating: "pg-13"
};

let api = `${giphy.baseURL}?api_key=${giphy.apiKey}&tag=${giphy.tag}&${giphy.rating}`;

console.log(api);

function callFetch() {
  fetch(api)
    .then(response => {
      return response.json();
    })
    .then(content => {

      gifContainer.innerHTML = (`
        <figure>
          <img 
            src="${content.data.images.downsized.url}" 
            alt="${content.data.title}"
            style="max-height: 15rem; max-width: 15rem;"
          >
          <figcaption style="text-align:center">${content.data.title}</figcaption>
        </figure>
      `);

      refreshRate();

    })
    .catch(err => {
      console.log(err);
    })
}

const refreshRate = () => {
  // Reset set intervals
  clearInterval(refresh);
  refresh = setInterval(function () {
    // Call Giphy API for new gif
    callFetch();
  }, duration);
}

callFetch();

// ************************************************ TASK TRACKER *************************************************

// ****** select items **********

// selecting form elements
const form = document.querySelector(".task-tracker-form");
const alert = document.querySelector(".alert");
const task = document.getElementById("task"); // task input
const submitBtn = document.querySelector(".submit-btn");

// selecting pending list elements
const container = document.querySelector(".pending-task-container");
const list = document.querySelector(".pending-task-list"); // list where tasks will be added
const clearBtn = document.querySelector(".clear-btn");

// selecting complete list elements
const completedContainer = document.querySelector(".completed-task-container");
const completedList = document.querySelector(".completed-task-list");
const completedClearBtn = document.querySelector(".completed-clear-btn");

// edit option
let editElement;
let editFlag = false;
let editID = "";

// ******** event listeners **********

form.addEventListener("submit", addItem); // submit form

clearBtn.addEventListener("click", clearItems); // clear list
completedClearBtn.addEventListener("click", clearCompletedItems); // clear list

window.addEventListener("DOMContentLoaded", checkAuth); // checks if correct user logged in

// AUTHENTICATION FUNCTION
function checkAuth() {
  let count = 0
  Object.keys(localStorage).forEach((key, index) => {
    if (JSON.parse(localStorage[key]).loggedIn === true) {
      CURRENT_USER = key;
      // alert(`Welcome ${JSON.parse(localStorage[key]).userName}`);

      window.onload = () => {
        const userName = JSON.parse(localStorage.getItem(CURRENT_USER)).userName;
        profileUserName.innerText = `[${userName}'s Profile]`;
        setupItems(); // display added items onload
        setupCompletedItems(); // display completed items on load
      }

      // window.addEventListener('DOMContentLoaded', profileUserName.innerText = `[${userName}'s Profile]`);
      // window.addEventListener("DOMContentLoaded", setupItems);//displayCompleted items onload
      // window.addEventListener("DOMContentLoaded", setupCompletedItems);//displayCompleted items onload
      count = count + 1;
    }
  })
  if (count === 0) {
    window.location.href = 'login.html';
  }
}

// ******** functions **********

// execute the below function when submit/edit button is clicked
function addItem(e) {
  e.preventDefault(); // prevents server request
  const value = task.value.trim(); // this gets the input value
  const id = new Date().getTime().toString();

  if (value.trim() !== "" && !editFlag/* i.e. editFlag === false (which is default value) */) {
    const element = document.createElement("article");
    let attr = document.createAttribute("data-id");
    attr.value = id;
    element.setAttributeNode(attr);
    element.classList.add("task-item");
    element.innerHTML = (`
      <p class="title">${value}</p> 
      <div class="btn-container">
        <!-- edit btn -->
        <button type="button" class="edit-btn">
          <i class="fas fa-edit"></i>
        </button>
        <!-- delete btn -->
        <button type="button" class="delete-btn">
          <i class="fas fa-trash"></i>
        </button>
        <button type="button" class="complete-btn">
          <i class="far fa-check-square"></i>
        </button>
      </div>
    `);

    // add event listeners to the buttons;
    const deleteBtn = element.querySelector(".delete-btn");
    deleteBtn.addEventListener("click", deleteItem);
    const editBtn = element.querySelector(".edit-btn");
    editBtn.addEventListener("click", editItem);
    const completeBtn = element.querySelector('.complete-btn');
    completeBtn.addEventListener("click", completedItem);

    list.appendChild(element); // append child

    displayAlert("task added to the list", "success"); // display alert

    container.classList.add("show-container"); // makes container visible

    addToLocalStorage(id, value); // set local storage

    setBackToDefault(); // set everything back to default

  } else if (value.trim() !== "" && editFlag) { // EDIT mode
    editElement.innerHTML = value.trim();
    displayAlert("task value changed", "success");

    editLocalStorage(editID, value); // edit  local storage
    setBackToDefault();
  } else { // when input box is blank
    displayAlert("please enter a task", "danger");
  }

}

// display alert
function displayAlert(text, action) {
  alert.textContent = text;
  alert.classList.add(`alert-${action}`);
  // remove alert
  setTimeout(function () {
    alert.textContent = "";
    alert.classList.remove(`alert-${action}`);
  }, 1000);
}

// call this func when clear items button is clicked
function clearItems() {
  const items = document.querySelectorAll(".task-item"); // selects all task items

  if (items.length > 0) {
    items.forEach(function (item) { // removes each item one by one
      list.removeChild(item);
    });
  }
  container.classList.remove("show-container"); // removes the show container class
  displayAlert("empty list", "danger");
  setBackToDefault();
  // localStorage.removeItem("list");
  let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
  parsedJsonObj.list = [];
  localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
}

function clearCompletedItems() {
  const items = document.querySelectorAll(".completed-task-item"); // selects all task items

  if (items.length > 0) {
    items.forEach(function (item) { // removes each item one by one
      completedList.removeChild(item);
    });
  }
  completedContainer.classList.remove("show-container"); // removes the show container class
  // localStorage.removeItem("completedList");
  let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
  parsedJsonObj.completedList = [];
  localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
}

// call this function when delete item button is clicked
function deleteItem(e) {
  const element = e.currentTarget.parentElement.parentElement; // item containing article
  const id = element.dataset.id;

  list.removeChild(element); // removes the entire article with it's text, btns etc

  if (list.children.length === 0) { // check if the entire list is empty
    container.classList.remove("show-container"); // if so, then remove this class
  }
  displayAlert("task removed", "danger");

  setBackToDefault();

  removeFromLocalStorage(id); // remove from local storage

}

function deleteCompletedItem(e) {
  const element = e.currentTarget.parentElement.parentElement; // item containing article
  const id = element.dataset.id;

  completedList.removeChild(element); // removes the entire article with it's text, btns etc

  if (completedList.children.length === 0) { // check if the entire list is empty
    completedContainer.classList.remove("show-container"); // if so, then remove this class
  }

  removeFromCompletedLocalStorage(id); // remove from completed local storage

}
// edit item
function editItem(e) {
  const element = e.currentTarget.parentElement.parentElement; // selects the article

  // set edit item
  editElement = e.currentTarget.parentElement.previousElementSibling;

  task.value = editElement.innerHTML; //sets input box value to the editElement^ value
  editFlag = true;
  editID = element.dataset.id;
  submitBtn.textContent = "edit"; // changes the text of the submit button
  // TODO check if I need below code
  // editLocalStorage();
}

// completed Item
function completedItem(e) {
  const pInnerText = e.currentTarget.parentElement.previousElementSibling.innerText;
  const id = new Date().getTime().toString();

  const element = document.createElement("article");
  let attr = document.createAttribute("data-id");
  attr.value = id;
  element.setAttributeNode(attr);
  element.classList.add("completed-task-item");
  element.innerHTML = (`
      <p class="title">${pInnerText}</p>
      <div class="btn-container">
        <!-- delete btn -->
        <button type="button" class="delete-btn">
          <i class="fas fa-trash"></i>
        </button>
      </div>
    `);

  const deleteBtn = element.querySelector(".delete-btn");
  deleteBtn.addEventListener("click", deleteCompletedItem);

  completedList.appendChild(element); // append child to completed list

  addToCompletedLocalStorage(id, pInnerText); // set local storage

  displayAlert("Task COMPLETED!", "success");

  // item containing article
  const elementToBeDeleted = e.currentTarget.parentElement.parentElement;
  const idToBeDeleted = elementToBeDeleted.dataset.id;

  list.removeChild(elementToBeDeleted); // removes the entire article with it's text, btns etc

  // if all the items in the list are completed then the list would be empty

  if (list.children.length === 0) { // check if the entire list is empty
    container.classList.remove("show-container"); // if so, then remove this class
  }

  removeFromLocalStorage(idToBeDeleted); // remove from list array of local storage

  completedContainer.classList.add("show-container"); // makes container visible

  setBackToDefault();

}

// set back to defaults
function setBackToDefault() {
  task.value = "";
  editFlag = false;
  editID = "";
  submitBtn.textContent = "submit";
}

// ****** local storage **********

// add to local storage
function addToLocalStorage(id, value) {
  const task = { id, value }; // sets a key value pair inside local storage
  let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
  parsedJsonObj.list.push(task);
  localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
}
function addToCompletedLocalStorage(id, value) {
  const task = { id, value };
  // let items = getCompletedLocalStorage();
  // items.push(task);
  // localStorage.setItem("completedList", JSON.stringify(items));
  let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
  parsedJsonObj.completedList.push(task);
  localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
}


function getLocalStorage() {
  // TODO check if i will need to pass "CURRENT_USER" with double quotes or without it
  return JSON.parse(localStorage.getItem(CURRENT_USER)).list;
}
function getCompletedLocalStorage() {
  return JSON.parse(localStorage.getItem(CURRENT_USER)).completedList;
}

function removeFromLocalStorage(id) {
  let items = getLocalStorage();

  items = items.filter(function (item) { // filter out item with respective id
    if (item.id !== id) {
      return item; // returns all items that don't have the respective id
    }
  });

  let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
  parsedJsonObj.list = [...items];
  localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
}
function removeFromCompletedLocalStorage(id) {
  let items = getCompletedLocalStorage();

  items = items.filter(function (item) {
    if (item.id !== id) {
      return item;
    }
  });

  // localStorage.setItem("completedList", JSON.stringify(items));
  let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
  parsedJsonObj.completedList = [...items];
  localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
}

function editLocalStorage(id, value) {
  let items = getLocalStorage();

  items = items.map(function (item) {
    if (item.id === id) {
      item.value = value;
    }
    return item;
  });
  // localStorage.setItem("list", JSON.stringify(items));
  let parsedJsonObj = JSON.parse(localStorage.getItem(CURRENT_USER));
  parsedJsonObj.list = [...items];
  localStorage.setItem(CURRENT_USER, JSON.stringify(parsedJsonObj));
}

// SETUP LOCALSTORAGE.REMOVEITEM('LIST');

// ****** setup items on reload **********

function setupItems() {
  let items = getLocalStorage();

  if (items.length > 0) {
    items.forEach(function (item) {
      createListItem(item.id, item.value);
    });
    container.classList.add("show-container");
  }
}
function setupCompletedItems() {
  let items = getCompletedLocalStorage();

  if (items.length > 0) {
    items.forEach(function (item) {
      createCompletedListItem(item.id, item.value);
    });
    completedContainer.classList.add("show-container");
  }
}

function createListItem(id, value) {
  const element = document.createElement("article");
  let attr = document.createAttribute("data-id");
  attr.value = id;
  element.setAttributeNode(attr);
  element.classList.add("task-item");
  element.innerHTML = (`
    <p class="title">${value}</p>
    <div class="btn-container">
      <!-- edit btn -->
      <button type="button" class="edit-btn">
        <i class="fas fa-edit"></i>
      </button>
      <!-- delete btn -->
      <button type="button" class="delete-btn">
        <i class="fas fa-trash"></i>
      </button>
      <button type="button" class="complete-btn">
        <i class="far fa-check-square"></i>
      </button>
    </div>
  `);
  // add event listeners to all buttons;
  const deleteBtn = element.querySelector(".delete-btn");
  deleteBtn.addEventListener("click", deleteItem);
  const editBtn = element.querySelector(".edit-btn");
  editBtn.addEventListener("click", editItem);
  const completeBtn = element.querySelector('.complete-btn');
  completeBtn.addEventListener("click", completedItem);

  // append child
  list.appendChild(element);
}
function createCompletedListItem(id, value) {
  const element = document.createElement("article");
  let attr = document.createAttribute("data-id");
  attr.value = id;
  element.setAttributeNode(attr);
  element.classList.add("completed-task-item");
  element.innerHTML = (`
    <p class="title">${value}</p>
    <div class="btn-container">
      <!-- delete btn -->
      <button type="button" class="delete-btn">
        <i class="fas fa-trash"></i>
      </button>
    </div>
  `);

  // add event listener to the delete button;
  const deleteBtn = element.querySelector(".delete-btn");
  deleteBtn.addEventListener("click", deleteCompletedItem);

  // append child
  completedList.appendChild(element);
}
