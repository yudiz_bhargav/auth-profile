const userName = document.getElementById('name');
const email = document.getElementById('email');
const mobileNumber = document.getElementById('mobile-number');
const gender = document.getElementById('gender');
const dob = document.getElementById('dob');
const password = document.getElementById('password');
const submitBtn = document.querySelector('.submit-btn');
const alert = document.querySelector('.alert');

// dob.addEventListener("onmouseover", function mouseOver() { dob.classList.toggle("dob-calendar-input") });

function isNumberKey(evt) {
  var charCode = (evt.which) ? evt.which : evt.keyCode
  return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

window.addEventListener("DOMContentLoaded", checkAuth);

function checkAuth() {
  let count = 0

  if (localStorage.length > 0) {
    Object.keys(localStorage).forEach((key, index) => {
      if (JSON.parse(localStorage[key]).loggedIn === false) {
        count = count + 1;
      }
    })
    if (count === 0) {
      window.location.href = 'dashboard.html';
    }
  }
}


submitBtn.addEventListener('click', (e) => {
  e.preventDefault();

  // No input field should be empty for registration form
  if ((userName.value.trim() && email.value.trim() && mobileNumber.value.trim() && gender.value.trim() && dob.value.trim() && password.value.trim()) === "") {
    console.log(userName.value, email.value, mobileNumber.value, gender.value, dob.value, password.value);
    displayAlert("Please enter all the fields to continue", 'danger');
  }
  else {
    const id = new Date().getTime().toString();

    //IMPORTANT---ALL INPUT VALUES ARE CONVERTED TO STRING BY DEFAULT
    currentUserInputValuesObject = {
      userName: userName.value,
      email: email.value,
      mobileNumber: mobileNumber.value,
      gender: gender.value,
      dob: dob.value,
      password: password.value,
      loggedIn: false,
      list: [],
      completedList: []
    }
    // console.log(currentUserInputValuesObject);

    ifElseChecker(currentUserInputValuesObject, id);
    // all the values need to be passed as one object for the second argument of addToLocalStorage() func
  }
})

function ifElseChecker(currentUserInputValuesObject, id) {

  let successfullyAddedCount = 0;

  // for (const [key, values] of Object.entries(currentUserInputValuesObject)) 
  for (let key of Object.keys(currentUserInputValuesObject)) {

    const editKEY = key;
    const value = currentUserInputValuesObject[`${key}`];

    // console.log(editKEY, value);


    // console.log(editKEY);
    if (editKEY === "userName") {
      successfullyAddedCount = successfullyAddedCount + 1;
      // console.log('done from username')
    }

    if (editKEY === "email") {
      let doesTheEmailExistCount = 0;
      Object.keys(localStorage).forEach((key, index) => {
        if (value === JSON.parse(localStorage[key]).email) {
          doesTheEmailExistCount = doesTheEmailExistCount + 1;
          displayAlert("Email is already in use", "danger");
        }
      })
      if (doesTheEmailExistCount === 0) {
        if (!validateEmail(value)) {
          displayAlert("Invalid email!", "danger");
        }
        else {
          successfullyAddedCount = successfullyAddedCount + 1;
          // console.log('done from email')
        }
      }
    }
    if (editKEY === "mobileNumber") {
      splittedValArray = value.split('');
      filteredSplittedValArray = splittedValArray.filter(el => {
        return Number.isNaN(el);
      });
      function isNumber(n) {
        return !isNaN(parseFloat(n)) && !isNaN(n - 0);
      }
      let doesTheNumberExistCount = 0;
      Object.keys(localStorage).forEach((key, index) => {
        if (value === JSON.parse(localStorage[key]).mobileNumber) {
          doesTheNumberExistCount = doesTheNumberExistCount + 1;
          displayAlert("Mobile Number is Taken", "danger");
        }
      })
      if (doesTheNumberExistCount === 0) {
        if (value.length !== 10) {
          displayAlert("Enter correct 10 digit mobile number!", "danger");
        }
        else if (((value[0] === '9') || (value[0] === '8') || (value[0] === '7')) && isNumber(value)) {
          successfullyAddedCount = successfullyAddedCount + 1;
        }
        else if (!isNumber(value)) {
          displayAlert("Enter only numeric values!", "danger");
        }
        else {
          displayAlert("please enter a valid mobile number");
        }
      }
    }
    if (editKEY.toLowerCase() === "gender") {
      if ((value.toLowerCase() === 'male') || (value.toLowerCase() === 'female')) {
        successfullyAddedCount = successfullyAddedCount + 1;
        // console.log('done from gender');
      }
      else {
        displayAlert("Only Male and Female Genders are allowed", "danger");
        // console.log('hi')
      }
    }
    if (editKEY === "dob") {
      const dobStringARRAY = value.split("-").map((el) => parseInt(el));
      // console.log(dobStringARRAY);
      filteredDobStringARRAY = dobStringARRAY.filter(el => {
        return Number.isNaN(el);
      })
      // console.log(filteredDobStringARRAY)
      function leapYear(year) {
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
      }
      if (filteredDobStringARRAY.length > 0) {
        displayAlert("DOB Must have only numeric values", "danger");
      }
      else if ((value.indexOf('-') !== 4) || (value.lastIndexOf('-') !== 7)) {
        displayAlert("Please enter DOB in YYYY-MM-DD format", "danger");
      }
      else if (parseInt(value.slice(0, 4)) < 1970) {
        displayAlert("Birth Year cannot be less than 1970", "danger");
      }
      else if (parseInt(value.slice(0, 4)) > 2005) {
        displayAlert("Birth Year cannot be greater than 2005", "danger");
      }
      else if (parseInt(value.slice(5, 7)) > 12) {
        displayAlert("Birth Month cannot be Greater than 12", "danger");
      }
      else if (parseInt(value.slice(5, 7)) < 01) {
        displayAlert("Birth Month cannot be less than 1", "danger");
      }
      else if (parseInt(value.slice(8,)) > 31) {
        displayAlert("Birth day cannot be greater than 31", "danger");
      }
      else if (parseInt(value.slice(8,)) < 01) {
        displayAlert("Birth day cannot be less than 1", "danger");
      }
      else if (((parseInt(value.slice(5, 7)) === 04) || (parseInt(value.slice(5, 7)) === 06) || (parseInt(value.slice(5, 7)) === 08) || (parseInt(value.slice(5, 7)) === 10) || (parseInt(value.slice(5, 7)) === 12)) && (parseInt(value.slice(8,)) > 30)) {
        displayAlert("Birth day cannot be greater than 30 for entered Month", "danger");
      }
      else if ((leapYear(parseInt(value.slice(0, 4)))) && (parseInt(value.slice(5, 7)) === 02) && (parseInt(value.slice(8,)) > 29)) {
        displayAlert("Birth day cannot be greater than 29 for entered Month & Year", "danger");
      }
      else if ((!leapYear(parseInt(value.slice(0, 4)))) && (parseInt(value.slice(5, 7)) === 02) && (parseInt(value.slice(8,)) > 28)) {
        displayAlert("Birth day cannot be greater than 28 for entered Month & Year", "danger");
      }
      else {
        successfullyAddedCount = successfullyAddedCount + 1;
        // console.log('done from dob');
      }
    }
    if (editKEY === "password") {
      const regex = /\d/;

      if (value.length < 8) {
        displayAlert("Password should have at least 8 characters and 1 number", 'danger');
        // console.log('minumum 7')
      }
      else if ((!regex.test(value))) {
        displayAlert("password should have Minimum one number", "danger");
        // console.log('minimum one num')
      }
      else {
        successfullyAddedCount = successfullyAddedCount + 1;
        // console.log('done from password');
      }
    }
  }
  if (successfullyAddedCount === 6) {
    addToLocalStorage(id, currentUserInputValuesObject);
    displayAlert("account CREATED!", 'success');
  }
  // else {
  //   displayAlert("Could not create account", 'danger');
  // }
}


// display alert function
function displayAlert(text, action) {
  alert.textContent = text;
  alert.classList.add(`alert-${action}`);
  // remove alert
  setTimeout(function () {
    alert.textContent = "";
    alert.classList.remove(`alert-${action}`);
  }, 3000);
}

function addToLocalStorage(inputID, inputUserValueObject) {
  // before adding to local storage, check if a user with same email/phone number already exists on local storage
  if (Object.keys(localStorage).length > 0) {
    let count = 0;

    Object.keys(localStorage).forEach((key, index) => {
      //CHECK IF TRIPLE QUOTES IS OKAY SINCE EVEN NUMERIC INPUT VALUES CAN BE A STRING
      // console.log(JSON.parse(localStorage[key]).mobileNumber);
      if ((JSON.parse(localStorage[key]).mobileNumber === inputUserValueObject.mobileNumber) ||
        (JSON.parse(localStorage[key]).email === inputUserValueObject.email)) {
        count = count + 1;
        displayAlert("Entered Mobile Number/Email Already Taken!", 'danger');
        // break; for some reason you cannot user break inside forEach
      }
    });

    if (count === 0) {
      const encryptedPwd = encryptionFunction(inputUserValueObject.password);
      inputUserValueObject.password = encryptedPwd;

      //JSON objects cannot have single quotes, so DON'T USE BACKTIKS
      localStorage.setItem("user" + inputID + "Object", JSON.stringify(inputUserValueObject));
      displayAlert("Account Created!", 'success');
      window.location.href = "login.html";
    }
  }
  else {
    const encryptedPwd = encryptionFunction(inputUserValueObject.password);
    inputUserValueObject.password = encryptedPwd;

    //JSON objects cannot have single quotes, so DON'T USE BACKTIKS
    window.location.href = "login.html";
    localStorage.setItem("user" + inputID + "Object", JSON.stringify(inputUserValueObject));
    displayAlert("Account Created!", 'success');
  }
}

// const arr = [1, 2, 3];

// let parsedJsonObj = JSON.parse(localStorage.getItem("user1621801702177Object"));

// console.log(parsedJsonObj);

// parsedJsonObj.list = [...arr];

// localStorage.setItem("user1621801702177Object", JSON.stringify(parsedJsonObj));

// console.log(JSON.parse(localStorage.getItem("user1621947255839Object")));

function encryptionFunction(enteredPassword) {
  const passwordArray = enteredPassword.split('');
  let encryptedPasswordArray = [];

  passwordArray.forEach((letter) => {
    switch (letter) {
      case "a":
        encryptedPasswordArray.push('01');
        break;
      case "b":
        encryptedPasswordArray.push('02');
        break;
      case "c":
        encryptedPasswordArray.push('03');
        break;
      case "d":
        encryptedPasswordArray.push('04');
        break;
      case "e":
        encryptedPasswordArray.push('05');
        break;
      case "f":
        encryptedPasswordArray.push('06');
        break;
      case "g":
        encryptedPasswordArray.push('07');
        break;
      case "h":
        encryptedPasswordArray.push('08');
        break;
      case "i":
        encryptedPasswordArray.push('09');
        break;
      case "j":
        encryptedPasswordArray.push('10');
        break;
      case "k":
        encryptedPasswordArray.push('11');
        break;
      case "l":
        encryptedPasswordArray.push('12');
        break;
      case "m":
        encryptedPasswordArray.push('13');
        break;
      case "n":
        encryptedPasswordArray.push('14');
        break;
      case "o":
        encryptedPasswordArray.push('15');
        break;
      case "p":
        encryptedPasswordArray.push('16');
        break;
      case "q":
        encryptedPasswordArray.push('17');
        break;
      case "r":
        encryptedPasswordArray.push('18');
        break;
      case "s":
        encryptedPasswordArray.push('19');
        break;
      case "t":
        encryptedPasswordArray.push('20');
        break;
      case "u":
        encryptedPasswordArray.push('21');
        break;
      case "v":
        encryptedPasswordArray.push('22');
        break;
      case "w":
        encryptedPasswordArray.push('23');
        break;
      case "x":
        encryptedPasswordArray.push('24');
        break;
      case "y":
        encryptedPasswordArray.push('25');
        break;
      case "z":
        encryptedPasswordArray.push('26');
        break;

      case '1':
        encryptedPasswordArray.push('aa');
        break;
      case '2':
        encryptedPasswordArray.push('bb');
        break;
      case '3':
        encryptedPasswordArray.push('cc');
        break;
      case '4':
        encryptedPasswordArray.push('dd');
        break;
      case '5':
        encryptedPasswordArray.push('ee');
        break;
      case '6':
        encryptedPasswordArray.push('ff');
        break;
      case '7':
        encryptedPasswordArray.push('gg');
        break;
      case '8':
        encryptedPasswordArray.push('hh');
        break;
      case '9':
        encryptedPasswordArray.push('ii');
        break;
      default:
        encryptedPasswordArray.push(letter);
      // code block
    }
  })
  const encryptedPasswordString = encryptedPasswordArray.join(';');
  return encryptedPasswordString;
}