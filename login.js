const alert = document.querySelector('.alert');
const loginBtn = document.querySelector('.submit-btn');
const emailOrMobile = document.getElementById('email-or-mobile');
const password = document.getElementById('password');

window.addEventListener("DOMContentLoaded", checkAuth);

function checkAuth() {
  let count = 0

  if (localStorage.length > 0) {
    Object.keys(localStorage).forEach((key, index) => {
      if (JSON.parse(localStorage[key]).loggedIn === false) {
        count = count + 1;
      }
    })
    if (count === 0) {
      window.location.href = 'dashboard.html';
    }
  }
}

loginBtn.addEventListener('click', (e) => {
  e.preventDefault();

  if ((emailOrMobile.value && password.value) === "") {
    displayAlert("Please enter all the fields to continue", 'danger');
  }
  else {
    console.log(localStorage);
    if (Object.keys(localStorage).length === 0) {
      displayAlert("You don't have an account yet. Go to Sign-Up Page", 'danger');
    }
    if (Object.keys(localStorage).length > 0) {
      currentUserInputValuesObject = {
        emailOrMobile: emailOrMobile.value,
        password: password.value
      }
      checkValidity(currentUserInputValuesObject);
    }
  }
})

function displayAlert(text, action) {
  alert.textContent = text;
  alert.classList.add(`alert-${action}`);
  // remove alert
  setTimeout(function () {
    alert.textContent = "";
    alert.classList.remove(`alert-${action}`);
  }, 1000);
}

function checkValidity(input) {

  Object.keys(localStorage).forEach((key, index) => {
    if (((JSON.parse(localStorage[key]).mobileNumber === input.emailOrMobile) || (JSON.parse(localStorage[key]).email === input.emailOrMobile)) && (decryptionFunction(JSON.parse(localStorage[key]).password) === input.password)) {
      displayAlert("Successfully Logging IN!!", 'success');

      let currentUserObject = JSON.parse(localStorage.getItem(key));
      currentUserObject.loggedIn = true;
      localStorage.setItem(key, JSON.stringify(currentUserObject));
      console.log(localStorage);
      window.location.href = `dashboard.html`;
    }
    else {
      displayAlert("Please Enter Correct Credentials To Log In", "danger");
    }
  });
}





function decryptionFunction(encryptedPasswordStr) {
  const passwordArray = encryptedPasswordStr.split(';');
  let decryptedPasswordArray = [];

  passwordArray.forEach((letter) => {
    switch (letter) {
      case "01":
        decryptedPasswordArray.push('a');
        break;
      case "02":
        decryptedPasswordArray.push('b');
        break;
      case "03":
        decryptedPasswordArray.push('c');
        break;
      case "04":
        decryptedPasswordArray.push('d');
        break;
      case "05":
        decryptedPasswordArray.push('e');
        break;
      case "06":
        decryptedPasswordArray.push('f');
        break;
      case "07":
        decryptedPasswordArray.push('g');
        break;
      case "08":
        decryptedPasswordArray.push('h');
        break;
      case "09":
        decryptedPasswordArray.push('i');
        break;
      case "10":
        decryptedPasswordArray.push('j');
        break;
      case "11":
        decryptedPasswordArray.push('k');
        break;
      case "12":
        decryptedPasswordArray.push('l');
        break;
      case "13":
        decryptedPasswordArray.push('m');
        break;
      case "14":
        decryptedPasswordArray.push('n');
        break;
      case "15":
        decryptedPasswordArray.push('o');
        break;
      case "16":
        decryptedPasswordArray.push('p');
        break;
      case "17":
        decryptedPasswordArray.push('q');
        break;
      case "18":
        decryptedPasswordArray.push('r');
        break;
      case "19":
        decryptedPasswordArray.push('s');
        break;
      case "20":
        decryptedPasswordArray.push('t');
        break;
      case "21":
        decryptedPasswordArray.push('u');
        break;
      case "22":
        decryptedPasswordArray.push('v');
        break;
      case "23":
        decryptedPasswordArray.push('w');
        break;
      case "24":
        decryptedPasswordArray.push('x');
        break;
      case "25":
        decryptedPasswordArray.push('y');
        break;
      case "26":
        decryptedPasswordArray.push('z');
        break;
      case 'aa':
        decryptedPasswordArray.push('1');
        break;
      case 'bb':
        decryptedPasswordArray.push('2');
        break;
      case 'cc':
        decryptedPasswordArray.push('3');
        break;
      case 'dd':
        decryptedPasswordArray.push('4');
        break;
      case 'ee':
        decryptedPasswordArray.push('5');
        break;
      case 'ff':
        decryptedPasswordArray.push('6');
        break;
      case 'gg':
        decryptedPasswordArray.push('7');
        break;
      case 'hh':
        decryptedPasswordArray.push('8');
        break;
      case 'ii':
        decryptedPasswordArray.push('9');
        break;
      default:
        decryptedPasswordArray.push(letter);
      // code block
    }
  })
  const decryptedPasswordString = decryptedPasswordArray.join('');
  return decryptedPasswordString;
}